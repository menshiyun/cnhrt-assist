// ==UserScript==
// @icon         http://www.chinahrt.com/favicon.ico
// @name         CNHRT-ASSIST
// @version      0.1.13
// @match        *://videoadmin.chinahrt.com/videoPlay/play*
// @match        *://web.chinahrt.com/course/preview*
// @match        *://web.chinahrt.com/studying/selected_course*
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    if (/videoadmin.chinahrt.com\/videoPlay\/play/.test(document.URL) == true) {
        var loadedHandlerOld = window.loadedHandler;

        window.videoObject.autoplay = true;

        window.attrset.ifPauseBlur = false;

        window.removePauseBlur();
        window.setPauseBlur = window.removePauseBlur;

        window.loadedHandler = function () {
            loadedHandlerOld();

            window.player.videoMute();
        }
    }

    function calc_seconds(element) {
        var seconds = 0;

        for (var i = 0; i < element.length; i++) {
            var label_text = element[i].textContent;
            var start = label_text.lastIndexOf('(');
            var label_time = label_text.substr(start + 1, 8);
            var label_time_split = label_time.split(':');

            seconds += parseInt(label_time_split[0]) * 60 * 60;
            seconds += parseInt(label_time_split[1]) * 60;
            seconds += parseInt(label_time_split[2]);
        }

        return seconds;
    }

    if (/web.chinahrt.com\/course\/preview/.test(document.URL) == true) {
        var title_ul = document.getElementsByClassName('video-word')[0].children[0];
        var content = document.getElementsByClassName('class-word')[1];
        var total_seconds = 0;

        var label_li = content.getElementsByTagName('label');
        var a_li = content.getElementsByTagName('a');

        total_seconds += calc_seconds(label_li);
        total_seconds += calc_seconds(a_li);

        var total_element = document.createElement('li');
        total_element.setAttribute('class', 'title');
        total_element.setAttribute('style', 'color: red;');
        total_element.textContent = '课程时长：' + new Date(total_seconds * 1000).toISOString().substr(11, 8);

        title_ul.append(total_element);

        if (a_li.length > 0) {
            var spans = content.getElementsByTagName('span');

            total_seconds -= calc_seconds(spans);

            var remain_time = document.createElement('li');
            remain_time.setAttribute('class', 'title');
            remain_time.setAttribute('style', 'color: blue;');
            remain_time.textContent = '剩余时长：' + new Date(total_seconds * 1000).toISOString().substr(11, 8);

            title_ul.append(remain_time);
        }
    }

    if (/web.chinahrt.com\/studying\/selected_course/.test(document.URL) == true) {
        var exam_tab = document.getElementsByClassName('exam-statustab')[0];

        exam_tab.children[0].removeAttribute('class');
        exam_tab.children[1].setAttribute('class', 'currr');

        document.getElementById('learnFinish').value = '0';
        window.initKKpager('1', '0');
    }
})();
